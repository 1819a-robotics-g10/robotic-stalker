import gopigo as go
import time
import numpy as np
import cv2
import serial
import json
import os
import _thread
lB = 23
lG = 67
lR = 86
hB = 44
hG = 191
hR = 239
erosion = 5
trackbar_value = 9
blobSize = 0
def updateValue(new_value):
    global trackbar_value
    trackbar_value = new_value
    return
def updateValue1(new_value):
    # make sure to write the new value into the global variable
    global lB
    lB = new_value
    return
def updateValue2(new_value):
    # make sure to write the new value into the global variable
    global lG
    lG = new_value
    return
def updateValue3(new_value):
    # make sure to write the new value into the global variable
    global lR
    lR = new_value
    return
def updateValue4(new_value):
    # make sure to write the new value into the global variable
    global hB
    hB = new_value
    return
def updateValue5(new_value):
    # make sure to write the new value into the global variable
    global hG
    hG = new_value
    return
def updateValue6(new_value):
    # make sure to write the new value into the global variable
    global hR
    hR = new_value
    return
def updateValue7(new_value):
    global erosion
    erosion = new_value*2+1
    return
try:
    f = open('thresh.txt', 'r')
    lB = f.readlines([1])
    lG = f.readlines([2])
    lR = f.readlines([3])
    hB = f.readlines([4])
    hG = f.readlines([5])
    hR = f.readlines([6])
    trackbar_value = int(rida)*2+1
    rida2 = f.readlines()[7]
    erosion = int(rida2)*2+1
except:
    pass    
blobparams = cv2.SimpleBlobDetector_Params()
##blobparams.filterByArea = False
##blobparams.filterByCircularity = False
blobparams.minDistBetweenBlobs = 200
blobparams.filterByArea = True
blobparams.minArea = 100
blobparams.maxArea = 1000000
blobparams.filterByCircularity = False
blobparams.filterByConvexity = False
blobparams.filterByInertia = False
blobparams.filterByColor = False
detector = cv2.SimpleBlobDetector_create(blobparams)
cap = cv2.VideoCapture(0)
start_time = 0
#cv2.createTrackbar("First trackbar", "Trackbar", trackbar_value, 255, updateValue)
cv2.namedWindow("Camera")
#cv2.createTrackbar("First trackbar", "Camera", trackbar_value, 255, updateValue)
cv2.createTrackbar("lB", "Camera", lB, 255, updateValue1)
cv2.createTrackbar("lG", "Camera", lG, 255, updateValue2)
cv2.createTrackbar("lR", "Camera", lR, 255, updateValue3)
cv2.createTrackbar("hB", "Camera", hB, 255, updateValue4)
cv2.createTrackbar("hG", "Camera", hG, 255, updateValue5)
cv2.createTrackbar("hR", "Camera", hR, 255, updateValue6)
cv2.createTrackbar("erosion", "Camera", erosion, 31, updateValue7)
go.set_speed(30)
keskmine = 0
distance = 0
    
while True:
    ret, frame = cap.read()
    frame = cv2.resize(frame, (0,0), fx=0.5, fy=0.5)
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
##    r = [len(frame)-200, len(frame)-190, 0, len(frame[0])] #narrowing down the image
##    frame = frame[r[0]:r[1], r[2]:r[3]]
    fps = 1.0/(time.time()-start_time)
    #print(fps)
    start_time = time.time()
    #blob detection on thresh:
    ret, thresh = cv2.threshold(frame, trackbar_value, 255, cv2.THRESH_BINARY)
    lowerLimits = np.array([lB, lG, lR])
    upperLimits = np.array([hB, hG, hR])
    kernel = np.ones(erosion) #loome kerneli
##    kernel2 = np.ones(dilation)
    thresholded = cv2.inRange(frame, lowerLimits, upperLimits)
    #thresholdime
    mask_inv = cv2.bitwise_not(thresholded)
    erode = cv2.erode(thresholded,kernel,iterations=1)
##    dilate = cv2.dilate(thresholded,kernel2,iterations=1) 
    outimage = cv2.bitwise_and(frame, frame, mask = mask_inv)   
     #eroteme threshold image'i
    keypoints = detector.detect(erode)
    cv2.putText(frame, str(int(fps)), (5, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
    #take a band of 50 pixels with the width of whole image
    for point in keypoints:
##        blobSize = point.size
        cv2.putText(frame, str(int(point.pt[0])) + " " + str(int(point.pt[1])), (int(point.pt[0]), int(point.pt[1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        img_keypoints = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    cv2.imshow("Camera", frame)
    cv2.imshow("Thresh", thresholded)
    #print(keypoints)
    if len(keypoints) == 0:
        go.left_rot()
    if len(keypoints) > 0:
        try:
            ser.flush()
            serial_line = ser.readline()
            data = json.loads(serial_line) # Extract the distance value
            dist = data[distance]
            print(data)
        except Exception as e:
            print('ultrasonic jampsib')
        point = keypoints[0]
        if point.pt[0] > ((len(frame[0])/2)- 25) and point.pt[0] < ((len(frame[0])/2)+ 25):
            go.forward()
            time.sleep(2)
            if (distance > 32):
                go.set_speed(50)
                go.fwd()
            elif (distance < 28):
                go.set_speed(50)
                go.bwd()
            else:
                go.stop()
        elif (point.pt[0]) < 160:
            go.left_rot()
##            if point.pt[0] > ((len(frame[0])/2)-25) and point.pt[0] < ((len(frame[0])/2)+25):
##                go.forward()
        elif (point.pt[0]) > 160:
            go.right_rot()
##            if point.pt[0] > ((len(frame[0])/2)-25) and point.pt[0] < ((len(frame[0])/2)+25):
##                go.forward()
##>>>>>>> b1b83f2814eb8387336a4f791159bca881971cc6
##    print(data[distance])
##    print(data)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        go.stop()
        break
    
f = open('thresh.txt', 'w+')
f.write("\n" + str(lB) + "\n" + str(lG) + "\n" + str(lR) + "\n" + str(hB) + "\n" + str(hG) + "\n" + str(hR) + "\n" + str(erosion))
f.close()
cv2.destroyAllWindows()

